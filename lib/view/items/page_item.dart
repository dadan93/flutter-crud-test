import 'package:flutter/material.dart';

import '../../utils/sqf_helper.dart';
import '../widgets/input_decoration.dart';

class ItemPage extends StatefulWidget {
  const ItemPage({super.key});

  @override
  State<ItemPage> createState() => _ItemPageState();
}

class _ItemPageState extends State<ItemPage> {
  // All journals
  List<Map<String, dynamic>> _items = [];

  bool _isLoading = true;
  void _getItemsData() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _items = data;
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _getItemsData();
  }

  final TextEditingController _itemNameController = TextEditingController();
  final TextEditingController _itemIdController = TextEditingController();
  final TextEditingController _barcodeController = TextEditingController();

  void _showForm(int? id) async {
    if (id != null) {
      // id == null -> create new item
      // id != null -> update an existing item
      final existingItems = _items.firstWhere((element) => element['id'] == id);
      _itemIdController.text = existingItems['item_id'];
      _itemNameController.text = existingItems['item_name'];
      _barcodeController.text = existingItems['barcode'].toString();
    }

    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              scrollable: true,
              title: const Text("Add Item"),
              content: Container(
                color: Colors.transparent,
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10))),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      TextField(
                        controller: _itemIdController,
                        decoration: inputDecoration("Item Id"),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextField(
                        controller: _itemNameController,
                        decoration: inputDecoration("Item Name"),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextField(
                        controller: _barcodeController,
                        decoration: inputDecoration("Barcode"),
                        keyboardType: TextInputType.number,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: SizedBox(
                              // width: double.infinity,
                              height: 50,
                              child: ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context).pop(context);
                                },
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.grey,
                                ),
                                child: const Text("Batal"),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: SizedBox(
                              // width: double.infinity,
                              height: 50,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.redAccent,
                                ),
                                onPressed: () async {
                                  // Save new journal
                                  if (id == null) {
                                    await _addItem();
                                  }

                                  if (id != null) {
                                    await _updateItem(id);
                                  }

                                  // Clear the text fields
                                  _itemIdController.text = '';
                                  _itemNameController.text = '';
                                  _barcodeController.text = '';

                                  // Close the bottom sheet
                                  Navigator.of(context).pop();
                                },
                                child: Text(id == null ? 'Save' : 'Edit'),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

// Insert a new item to the database
  Future<void> _addItem() async {
    await SQLHelper.createItem(_itemIdController.text, _itemNameController.text,
        int.parse(_barcodeController.text));
    _getItemsData();
  }

  // Update an existing item
  Future<void> _updateItem(int id) async {
    await SQLHelper.updateItem(id, _itemIdController.text,
        _itemNameController.text, int.parse(_barcodeController.text));
    _getItemsData();
  }

  // Delete an item
  void _deleteItem(int id) async {
    await SQLHelper.deleteItem(id);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Successfully deleted a Item!'),
    ));
    _getItemsData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Item", style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _showForm(null),
        child: const Icon(Icons.add),
      ),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              margin: const EdgeInsets.all(16),
              child: ListView.builder(
                itemCount: _items.length,
                itemBuilder: (context, index) => Card(
                  color: Colors.orange[200],
                  margin: const EdgeInsets.all(15),
                  child: ListTile(
                      isThreeLine: true,
                      title: Text(_items[index]['item_id']),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(_items[index]['item_name']),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(_items[index]['barcode'].toString()),
                        ],
                      ),
                      trailing: SizedBox(
                        width: 100,
                        child: Row(
                          children: [
                            IconButton(
                              icon: const Icon(Icons.edit),
                              onPressed: () => _showForm(_items[index]['id']),
                            ),
                            IconButton(
                              icon: const Icon(Icons.delete),
                              onPressed: () => _deleteItem(_items[index]['id']),
                            ),
                          ],
                        ),
                      )),
                ),
              ),
            ),
    );
  }
}
