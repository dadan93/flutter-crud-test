import 'package:flutter/material.dart';

import '../../utils/sqf_helper.dart';
import '../widgets/input_decoration.dart';

class SearchItemPage extends StatefulWidget {
  const SearchItemPage({super.key});

  @override
  State<SearchItemPage> createState() => _SearchItemPageState();
}

class _SearchItemPageState extends State<SearchItemPage> {
  List<Map<String, dynamic>> _items = [];
  List<Map<String, dynamic>> usersFiltered = [];
  bool _isLoading = true;
  String _searchResult = '';
  final TextEditingController _searchController = TextEditingController();

  void _getItemsData() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _items = data;
      usersFiltered = data;
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _getItemsData();
  }

  @override
  Widget build(BuildContext context) {
    print("Data Item $_items");
    print("Data filter $usersFiltered");
    return Scaffold(
      appBar: AppBar(
        title: const Text("Search", style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
      ),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              margin: const EdgeInsets.all(16),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(children: [
                  SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: TextFormField(
                      controller: _searchController,
                      decoration: inputDecoration("Search Item"),
                      onChanged: (value) {
                        setState(() {
                          _searchResult = value;

                          usersFiltered = _items
                              .where((data) =>
                                  data['item_id']
                                      .toString()
                                      .toLowerCase()
                                      .contains(_searchResult.toLowerCase()) ||
                                  data['item_name']
                                      .toString()
                                      .toLowerCase()
                                      .contains(_searchResult.toLowerCase()) ||
                                  data['barcode']
                                      .toString()
                                      .toLowerCase()
                                      .contains(_searchResult.toLowerCase()))
                              .toList();
                          print(_items.where((data) => data['item_id']
                              .toString()
                              .contains(_searchResult)));
                          print(
                              " datda data${_items.where((data) => data['item_id'].toString().contains(_searchResult))}");
                        });
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      border: TableBorder.all(color: Colors.grey),
                      columns: const <DataColumn>[
                        DataColumn(
                          label: Text(
                            'ItemId',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'ItemName',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'Barcode',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                      rows: List.generate(
                        usersFiltered.length,
                        (index) => DataRow(
                          cells: <DataCell>[
                            DataCell(Text(usersFiltered[index]['item_id'])),
                            DataCell(Text(usersFiltered[index]['item_name'])),
                            DataCell(Text(
                                usersFiltered[index]['barcode'].toString())),
                          ],
                        ),
                      ),
                    ),
                  )
                ]),
              ),
            ),
    );
  }
}
